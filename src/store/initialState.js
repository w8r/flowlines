export default {

  // points
  points: [
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Wald (BE)",
        "GEMFLAECHE": 1330,
        "SEEFLAECHE": -29998,
        "id": "ac75d547-48d6-49c8-b872-6b44156063b0",
        "name": "Wald (BE)",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          7.487220344006566,
          46.86765685333527
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Horw",
        "GEMFLAECHE": 2043,
        "SEEFLAECHE": 757,
        "id": "1be2535b-6340-40ea-9c49-4a7fb557fbe1",
        "name": "Horw",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.340405485017126,
          47.00535321750938
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Gossau (ZH)",
        "GEMFLAECHE": 1826,
        "SEEFLAECHE": -29998,
        "id": "7fb485cd-61d6-43c8-8c22-f2ce43a9255e",
        "name": "Gossau (ZH)",
        "radius": 200,
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.759241826967763,
          47.30598737435784
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Boltigen",
        "GEMFLAECHE": 7707,
        "SEEFLAECHE": -29998,
        "id": "b008e5df-1c58-41be-8e7d-5710555afbc2",
        "name": "Boltigen",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          7.369327882880027,
          46.57356615813108
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Lens",
        "GEMFLAECHE": 1398,
        "SEEFLAECHE": -29998,
        "id": "fdfba186-dfa6-4ea8-9be3-5ac92d9a1c5a",
        "name": "Lens",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          7.473057758343313,
          46.307658026802024
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Laax",
        "GEMFLAECHE": 3171,
        "SEEFLAECHE": -29998,
        "id": "ed30abf5-ee98-4bd7-9a73-9abefc4d5bd6",
        "name": "Laax",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          9.236465641219176,
          46.82966127876096
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Erlinsbach (SO)",
        "GEMFLAECHE": 887,
        "SEEFLAECHE": -29998,
        "id": "253992c4-90f2-4082-9f6c-8d830fbd9b81",
        "name": "Erlinsbach (SO)",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          7.975110355585619,
          47.40564864301332
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "La Sagne",
        "GEMFLAECHE": 2555,
        "SEEFLAECHE": -29998,
        "id": "d9ac2b9c-3f36-46bc-84f2-dc5b21c068cb",
        "name": "La Sagne",
        "selected": true
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          6.8174849454153446,
          47.028006733225055
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Moudon",
        "GEMFLAECHE": 1565,
        "SEEFLAECHE": -29998,
        "id": "4b3f22d0-8704-4a72-a683-f723e2c4ca88",
        "name": "Moudon",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          6.763545410174436,
          46.67045597929868
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Tujetsch",
        "GEMFLAECHE": 13391,
        "SEEFLAECHE": -29998,
        "id": "2f7871b8-3f1a-4a6f-9b1d-efc5f48d3c56",
        "name": "Tujetsch",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.771755799143655,
          46.656218350619014
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Serravalle",
        "GEMFLAECHE": 9692,
        "SEEFLAECHE": -29998,
        "id": "b44ccf73-2c44-4b84-81cf-e7efd4387537",
        "name": "Serravalle",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          9.04975186538526,
          46.45649778988109
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Bütschwil-Ganterschwil",
        "GEMFLAECHE": 2183,
        "SEEFLAECHE": -29998,
        "id": "e8a7e05f-e364-401d-a679-b37dd8a1e333",
        "name": "Bütschwil-Ganterschwil",
        "selected": true
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          9.066348645459387,
          47.329772960203655
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Schöftland",
        "GEMFLAECHE": 628,
        "SEEFLAECHE": -29998,
        "id": "f0e627ba-8839-410d-8378-b65beab74ce4",
        "name": "Schöftland",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.03734828086359,
          47.30164315485815
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Tavannes",
        "GEMFLAECHE": 1478,
        "SEEFLAECHE": -29998,
        "id": "b83d6437-ffe9-4380-8e87-a9f76ad392af",
        "name": "Tavannes",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          7.190912497083175,
          47.214345585840434
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Meiringen",
        "GEMFLAECHE": 4063,
        "SEEFLAECHE": -29998,
        "id": "870498d5-b06e-4f33-89e9-1ac5236b146a",
        "name": "Meiringen",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.18257010651219,
          46.72168039725876
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Realp",
        "GEMFLAECHE": 7788,
        "SEEFLAECHE": -29998,
        "id": "6d933b25-6814-4d72-84ac-2255e4ddacf2",
        "name": "Realp",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.460566172753797,
          46.56500873119828
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Oberägeri",
        "GEMFLAECHE": 3624,
        "SEEFLAECHE": 617,
        "id": "3be45171-83f7-4724-912c-23eb40ce01af",
        "name": "Oberägeri",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          8.663876728661837,
          47.13537244497445
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "GEMNAME": "Oeschenbach",
        "GEMFLAECHE": 392,
        "SEEFLAECHE": -29998,
        "id": "1a90a48f-19ae-41ff-9a3b-b53a58bc4368",
        "name": "Oeschenbach",
        "selected": false
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          7.75105382458492,
          47.10713928742966
        ]
      }
    }
  ],

  // map data
  data: {
    'type': 'FeatureCollection',
    'features': []
  },

  // map extent
  extent: [[-1, -1], [1, 1]],

  // selected points
  currentPoints: [],

  // hash map
  
  "flowLinesMap": {
    "1be2535b-6340-40ea-9c49-4a7fb557fbe1_b83d6437-ffe9-4380-8e87-a9f76ad392af": 0,
    "b83d6437-ffe9-4380-8e87-a9f76ad392af_1be2535b-6340-40ea-9c49-4a7fb557fbe1": 0,
    "f0e627ba-8839-410d-8378-b65beab74ce4_7fb485cd-61d6-43c8-8c22-f2ce43a9255e": 1,
    "7fb485cd-61d6-43c8-8c22-f2ce43a9255e_f0e627ba-8839-410d-8378-b65beab74ce4": 1,
    "1be2535b-6340-40ea-9c49-4a7fb557fbe1_e8a7e05f-e364-401d-a679-b37dd8a1e333": 2,
    "e8a7e05f-e364-401d-a679-b37dd8a1e333_1be2535b-6340-40ea-9c49-4a7fb557fbe1": 2,
    "1be2535b-6340-40ea-9c49-4a7fb557fbe1_253992c4-90f2-4082-9f6c-8d830fbd9b81": 3,
    "253992c4-90f2-4082-9f6c-8d830fbd9b81_1be2535b-6340-40ea-9c49-4a7fb557fbe1": 3
  },

  "flowLines": {
    "type": "FeatureCollection",
    "map": {},
    "features": [
      {
        "type": "Feature",
        "properties": {
          "b83d6437-ffe9-4380-8e87-a9f76ad392af": 2,
          "1be2535b-6340-40ea-9c49-4a7fb557fbe1": 4,
          "p1": "b83d6437-ffe9-4380-8e87-a9f76ad392af",
          "p2": "1be2535b-6340-40ea-9c49-4a7fb557fbe1"
        },
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [
              7.190912497083175,
              47.214345585840434
            ],
            [
              8.340405485017126,
              47.00535321750938
            ]
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "7fb485cd-61d6-43c8-8c22-f2ce43a9255e": 10,
          "f0e627ba-8839-410d-8378-b65beab74ce4": 8,
          "p1": "7fb485cd-61d6-43c8-8c22-f2ce43a9255e",
          "p2": "f0e627ba-8839-410d-8378-b65beab74ce4"
        },
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [
              8.759241826967763,
              47.30598737435784
            ],
            [
              8.03734828086359,
              47.30164315485815
            ]
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "e8a7e05f-e364-401d-a679-b37dd8a1e333": 5,
          "1be2535b-6340-40ea-9c49-4a7fb557fbe1": 3,
          "p1": "e8a7e05f-e364-401d-a679-b37dd8a1e333",
          "p2": "1be2535b-6340-40ea-9c49-4a7fb557fbe1"
        },
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [
              9.066348645459387,
              47.329772960203655
            ],
            [
              8.340405485017126,
              47.00535321750938
            ]
          ]
        }
      },
      {
        "type": "Feature",
        "properties": {
          "253992c4-90f2-4082-9f6c-8d830fbd9b81": 3,
          "1be2535b-6340-40ea-9c49-4a7fb557fbe1": 3,
          "p1": "253992c4-90f2-4082-9f6c-8d830fbd9b81",
          "p2": "1be2535b-6340-40ea-9c49-4a7fb557fbe1"
        },
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [
              7.975110355585619,
              47.40564864301332
            ],
            [
              8.340405485017126,
              47.00535321750938
            ]
          ]
        }
      }
    ]
  }
}
